﻿namespace SimpleNetMeter
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.Adapter = new System.Windows.Forms.Label();
            this.AdapterLeft = new System.Windows.Forms.Button();
            this.AdapterRight = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.StartDate = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.CountToday = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.CountAll = new System.Windows.Forms.Label();
            this.Zero = new System.Windows.Forms.Button();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.megnyitásToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.beKikapcsolásToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kilépésToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.label2 = new System.Windows.Forms.Label();
            this.Indicator = new System.Windows.Forms.PictureBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.wifi_name = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Indicator)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Hálózati adapter";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.Adapter);
            this.panel1.Location = new System.Drawing.Point(12, 53);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(342, 39);
            this.panel1.TabIndex = 3;
            // 
            // Adapter
            // 
            this.Adapter.AutoSize = true;
            this.Adapter.Location = new System.Drawing.Point(3, 14);
            this.Adapter.Name = "Adapter";
            this.Adapter.Size = new System.Drawing.Size(44, 13);
            this.Adapter.TabIndex = 0;
            this.Adapter.Text = "Adapter";
            // 
            // AdapterLeft
            // 
            this.AdapterLeft.Location = new System.Drawing.Point(360, 55);
            this.AdapterLeft.Name = "AdapterLeft";
            this.AdapterLeft.Size = new System.Drawing.Size(19, 39);
            this.AdapterLeft.TabIndex = 4;
            this.AdapterLeft.Text = "<";
            this.AdapterLeft.UseVisualStyleBackColor = true;
            this.AdapterLeft.Click += new System.EventHandler(this.AdapterLeft_Click);
            // 
            // AdapterRight
            // 
            this.AdapterRight.Location = new System.Drawing.Point(381, 55);
            this.AdapterRight.Name = "AdapterRight";
            this.AdapterRight.Size = new System.Drawing.Size(19, 39);
            this.AdapterRight.TabIndex = 5;
            this.AdapterRight.Text = ">";
            this.AdapterRight.UseVisualStyleBackColor = true;
            this.AdapterRight.Click += new System.EventHandler(this.AdapterRight_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 134);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Kezdete";
            // 
            // StartDate
            // 
            this.StartDate.AutoSize = true;
            this.StartDate.Location = new System.Drawing.Point(68, 134);
            this.StartDate.Name = "StartDate";
            this.StartDate.Size = new System.Drawing.Size(10, 13);
            this.StartDate.TabIndex = 7;
            this.StartDate.Text = "-";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 156);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(22, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Ma";
            // 
            // CountToday
            // 
            this.CountToday.AutoSize = true;
            this.CountToday.Location = new System.Drawing.Point(68, 156);
            this.CountToday.Name = "CountToday";
            this.CountToday.Size = new System.Drawing.Size(65, 13);
            this.CountToday.TabIndex = 9;
            this.CountToday.Text = "CountToday";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(9, 178);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 13);
            this.label7.TabIndex = 10;
            this.label7.Text = "Összesen";
            // 
            // CountAll
            // 
            this.CountAll.AutoSize = true;
            this.CountAll.Location = new System.Drawing.Point(68, 178);
            this.CountAll.Name = "CountAll";
            this.CountAll.Size = new System.Drawing.Size(46, 13);
            this.CountAll.TabIndex = 11;
            this.CountAll.Text = "CountAll";
            // 
            // Zero
            // 
            this.Zero.Location = new System.Drawing.Point(12, 207);
            this.Zero.Name = "Zero";
            this.Zero.Size = new System.Drawing.Size(75, 23);
            this.Zero.TabIndex = 12;
            this.Zero.Text = "Nullázás";
            this.Zero.UseVisualStyleBackColor = true;
            this.Zero.Click += new System.EventHandler(this.Zero_Click);
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.BalloonTipText = "A program a háttérben fut.";
            this.notifyIcon1.BalloonTipTitle = "SimpleNetMeter";
            this.notifyIcon1.ContextMenuStrip = this.contextMenuStrip1;
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "notifyIcon1";
            this.notifyIcon1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon1_MouseDoubleClick);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.megnyitásToolStripMenuItem,
            this.beKikapcsolásToolStripMenuItem,
            this.kilépésToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(154, 70);
            // 
            // megnyitásToolStripMenuItem
            // 
            this.megnyitásToolStripMenuItem.Name = "megnyitásToolStripMenuItem";
            this.megnyitásToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.megnyitásToolStripMenuItem.Text = "Megnyitás";
            this.megnyitásToolStripMenuItem.Click += new System.EventHandler(this.megnyitásToolStripMenuItem_Click);
            // 
            // beKikapcsolásToolStripMenuItem
            // 
            this.beKikapcsolásToolStripMenuItem.Name = "beKikapcsolásToolStripMenuItem";
            this.beKikapcsolásToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.beKikapcsolásToolStripMenuItem.Text = "Be/Kikapcsolás";
            this.beKikapcsolásToolStripMenuItem.Click += new System.EventHandler(this.beKikapcsolásToolStripMenuItem_Click);
            // 
            // kilépésToolStripMenuItem
            // 
            this.kilépésToolStripMenuItem.Name = "kilépésToolStripMenuItem";
            this.kilépésToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.kilépésToolStripMenuItem.Text = "Kilépés";
            this.kilépésToolStripMenuItem.Click += new System.EventHandler(this.kilépésToolStripMenuItem_Click);
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.backgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(384, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(28, 13);
            this.label2.TabIndex = 15;
            this.label2.Text = "v1.3";
            // 
            // Indicator
            // 
            this.Indicator.Image = global::SimpleNetMeter.Properties.Resources.indicator_off;
            this.Indicator.Location = new System.Drawing.Point(104, 12);
            this.Indicator.Name = "Indicator";
            this.Indicator.Size = new System.Drawing.Size(17, 17);
            this.Indicator.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Indicator.TabIndex = 16;
            this.Indicator.TabStop = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 13);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(76, 13);
            this.label4.TabIndex = 17;
            this.label4.Text = "Mérés állapota";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 107);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(89, 13);
            this.label6.TabIndex = 18;
            this.label6.Text = "Wifi hálózat neve";
            // 
            // wifi_name
            // 
            this.wifi_name.AutoSize = true;
            this.wifi_name.Location = new System.Drawing.Point(198, 107);
            this.wifi_name.Name = "wifi_name";
            this.wifi_name.Size = new System.Drawing.Size(19, 13);
            this.wifi_name.TabIndex = 19;
            this.wifi_name.Text = "----";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(104, 102);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 20;
            this.button1.Text = "Beállítás";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(427, 245);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.wifi_name);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.Zero);
            this.Controls.Add(this.Indicator);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.AdapterRight);
            this.Controls.Add(this.CountAll);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.CountToday);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.StartDate);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.AdapterLeft);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "SimpleNetMeter";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Resize += new System.EventHandler(this.Form1_Resize);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Indicator)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label Adapter;
        private System.Windows.Forms.Button AdapterLeft;
        private System.Windows.Forms.Button AdapterRight;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label StartDate;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label CountToday;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label CountAll;
        private System.Windows.Forms.Button Zero;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem beKikapcsolásToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem megnyitásToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem kilépésToolStripMenuItem;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox Indicator;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label wifi_name;
        private System.Windows.Forms.Button button1;
    }
}

