﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PcapDotNet.Core;
using System.IO;
using System.Runtime.InteropServices;
using NativeWifi;

namespace SimpleNetMeter
{

    public partial class Form1 : Form
    {

        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            MessageBox.Show((e.ExceptionObject as Exception).Message, "Unhandled UI Exception");
            // here you can log the exception ...
        }

        static Form1 the_form;

        [DllImport("kernel32.dll")]
        static extern bool FreeConsole();

        const String SETTINGS_FILE = "Settings.txt";
        const UInt64 GB = 1024 * 1024 * 1024;

        PacketCommunicator communicator;
        IList<LivePacketDevice> allDevices;

        Icon ON_ICON = SimpleNetMeter.Properties.Resources.ON;
        Icon OFF_ICON = SimpleNetMeter.Properties.Resources.OFF;

        Image indicator_on = SimpleNetMeter.Properties.Resources.indicator_on;
        Image indicator_off = SimpleNetMeter.Properties.Resources.indicator_off;

        CountData count_data = new CountData();

        bool started = false;
        DateTime start_time_p = DateTime.Now;
        DateTime start_time
        {
            set
            {
                start_time_p = value;
                StartDate.Text = start_time_p.ToString("d");
            }
            get
            {
                return start_time_p;
            }
        }

        // last displayed
        UInt64 disp_count_today = 0;
        UInt64 icon_disp_count = 0; //all
        UInt64 disp_count_all = 0;

        private bool meter_on_private = false;

        bool meter_on
        {
            set
            {
                meter_on_private = value;
                if (value)
                {
                    Indicator.Image = indicator_on;
                    if (!backgroundWorker1.IsBusy)
                    {
                        backgroundWorker1.RunWorkerAsync(null);
                        notifyIcon1.Icon = ON_ICON;
                        Zero.Enabled = false;
                        button1.Enabled = false;
                        AdapterLeft.Enabled = false;
                        AdapterRight.Enabled = false;

                        if (!started)
                            start_time = DateTime.Now;
                    }
                }
                else
                {
                    Indicator.Image = indicator_off;
                    notifyIcon1.Icon = OFF_ICON;
                }
            }
            get
            {
                return meter_on_private;
            }
        }

        int cur_adapter = 0;

        public Form1()
        {
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
            //Environment.SpecialFolder.ApplicationData
            the_form = this;

            FreeConsole();        

            InitializeComponent();

            //Start minimized
            string[] args = Environment.GetCommandLineArgs();
            foreach (string arg in args)
            {
                string str = arg.ToLower();
                if (str == "--minimized")
                {
                    this.WindowState = FormWindowState.Minimized;
                }
            }

            searchAdapters();

            readFile();

            count_data.count_today = 0;
            updateCountLabels();

            string w_name;
            if (Wifi.checkWifi(out w_name))
            {
                if (w_name == wifi_name.Text)
                {
                    meter_on = true;
                }
            }
            //Wifi.registerWlanListener();

            timer1.Start();
        }

        private int searchAdapters()
        {
            allDevices = LivePacketDevice.AllLocalMachine;

            if (allDevices.Count == 0)
            {
                MessageBox.Show("No interfaces found! Make sure WinPcap is installed.");
                return -1;
            }

            LivePacketDevice device = allDevices[cur_adapter];
            if (device.Description != null)
                Adapter.Text = cur_adapter.ToString() + " " + device.Description;

            return 0;
        }

        private void quitApp()
        {
            saveFile();
            //Wifi.unregisterWlanListener();
            Application.Exit();
        }

        private void readFile()
        {
            try
            {
                using (StreamReader reader = new StreamReader(SETTINGS_FILE))
                {
                    string line;
                    int line_cnt = 0;
                    int adapt;
                    while ((line = reader.ReadLine()) != null)
                    {
                        if (line_cnt == 0)
                        {
                            adapt = int.Parse(line);
                            if (adapt < allDevices.Count)
                            {
                                cur_adapter = adapt;
                                Adapter.Text = cur_adapter.ToString() + " " + allDevices[cur_adapter].Description;
                            }
                            else
                                MessageBox.Show("Error: readFile: adapt < allDevices.Count is false.");
                        }
                        if (line_cnt == 1)
                        {
                            start_time = DateTime.Parse(line);
                            started = true; //we have a start date already
                        }
                        if (line_cnt == 2)
                            count_data.count_all = UInt64.Parse(line);
                        if (line_cnt == 3)
                            wifi_name.Text = line;
                        line_cnt++;
                    }
                }
            }
            catch (FileNotFoundException e)
            {
                //The file doesn't exist
                cur_adapter = 0;
                count_data.count_all = 0;
            }
                
        }

        private void saveFile()
        {
            try
            {
                using (StreamWriter writer = new StreamWriter(SETTINGS_FILE))
                {
                    writer.WriteLine(cur_adapter.ToString());
                    writer.WriteLine(start_time.ToString());
                    writer.WriteLine(count_data.count_all.ToString());
                    writer.WriteLine(wifi_name.Text);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Write exception: " + e.ToString() );
            }
        }

        private void AdapterLeft_Click(object sender, EventArgs e)
        {
            cur_adapter--;
            if (cur_adapter < 0)
                cur_adapter = allDevices.Count - 1;

            LivePacketDevice device = allDevices[cur_adapter];
            if (device.Description != null)
                Adapter.Text = cur_adapter.ToString() + " " + device.Description;
        }
        private void AdapterRight_Click(object sender, EventArgs e)
        {
            cur_adapter++;
            if (cur_adapter >= allDevices.Count)
                cur_adapter = 0;
            Adapter.Text = cur_adapter.ToString();

            LivePacketDevice device = allDevices[cur_adapter];
            if (device.Description != null)
                Adapter.Text = cur_adapter.ToString() + " " + device.Description;
        }
        private void Form1_Resize(object sender, EventArgs e)
        {
            if (FormWindowState.Minimized == this.WindowState)
            {
                notifyIcon1.Visible = true;

                //íif (show_tip)
                    //notifyIcon1.ShowBalloonTip(500);

                this.Hide();
            }
            else if (FormWindowState.Normal == this.WindowState)
            {
                notifyIcon1.Visible = false;
            }
        }
        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            //meter_on = !meter_on;
            this.Show();
            this.WindowState = FormWindowState.Normal;
        }
        private void kilépésToolStripMenuItem_Click(object sender, EventArgs e)
        {
            quitApp();
        }
        private void beKikapcsolásToolStripMenuItem_Click(object sender, EventArgs e)
        {
            /*meter_on = !meter_on;
            CheckBox chb = Controls["MeterOnOff"] as CheckBox;
            if (chb != null)
            {
                chb.Checked = meter_on;
            }*/
        }
        private void megnyitásToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Show();
            this.WindowState = FormWindowState.Normal;
        }
        private void Zero_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Biztos benne?", "Nullázás", MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                count_data.count_today = 0;
                count_data.count_all = 0;
                start_time = DateTime.Now;
            }

        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            // Take the selected adapter
            PacketDevice selectedOutputDevice = allDevices[cur_adapter];

            // Open the output adapter
            using (
            communicator = selectedOutputDevice.Open(100, PacketDeviceOpenAttributes.Promiscuous,
                                                                        1000))
            {
                // Compile and set the filter
                communicator.SetFilter("tcp");

                // Put the interface in statstics mode
                communicator.Mode = PacketCommunicatorMode.Statistics;

                Console.WriteLine("TCP traffic summary:");

                // Start the main loop
                communicator.ReceiveStatistics(0, StatisticsHandler);
            }
        }


        private void StatisticsHandler(PacketSampleStatistics statistics)
        {
            count_data.count_today += statistics.AcceptedBytes;
            count_data.count_all += statistics.AcceptedBytes;

            if( !meter_on )
                communicator.Break();
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Zero.Enabled = true;
            button1.Enabled = true;
            AdapterLeft.Enabled = true;
            AdapterRight.Enabled = true;
        }


        private void updateCountLabels()
        {

            double in_mb = count_data.count_all % (GB) / 1024.0 / 1024.0;
            UInt64 in_gb = count_data.count_all / (GB);
            if (in_gb != 0)
                notifyIcon1.Text = in_gb + " GB | " + in_mb.ToString("0") + " MB";
            else
                notifyIcon1.Text = in_mb.ToString("0") + " MB";

            icon_disp_count = count_data.count_all;

            in_mb = count_data.count_today % (GB) / 1024.0 / 1024.0;
            in_gb = count_data.count_today / (GB);
            if (in_gb != 0)
                CountToday.Text = in_gb.ToString() + " GB | " + in_mb.ToString("0") + " MB";
            else
                CountToday.Text = in_mb.ToString("0.0") + " MB";

            disp_count_today = count_data.count_today;

            in_mb = count_data.count_all % (GB) / 1024.0 / 1024.0;
            in_gb = count_data.count_all / (GB);
            if (in_gb != 0)
                CountAll.Text = in_gb + " GB | " + in_mb.ToString("0") + " MB";
            else
                CountAll.Text = in_mb.ToString("0") + " MB";

            disp_count_all = count_data.count_all;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if ( FormWindowState.Minimized == this.WindowState
                && icon_disp_count != count_data.count_all)
            {
                double in_mb = count_data.count_all % (GB) / 1024.0 / 1024.0;
                UInt64 in_gb = count_data.count_all / (GB);
                if (in_gb != 0)
                    notifyIcon1.Text = in_gb + " GB | " + in_mb.ToString("0") + " MB";
                else
                    notifyIcon1.Text = in_mb.ToString("0") + " MB";

                icon_disp_count = count_data.count_all;
            }

            //refresh label
            if (
                FormWindowState.Minimized != this.WindowState &&
                disp_count_today != count_data.count_today)
            {
                double in_mb = count_data.count_today % (GB) / 1024.0 / 1024.0;
                UInt64 in_gb = count_data.count_today / (GB);
                if (in_gb != 0)
                    CountToday.Text = in_gb.ToString() + " GB | " + in_mb.ToString("0") + " MB";
                else
                    CountToday.Text = in_mb.ToString("0.0") + " MB";

                disp_count_today = count_data.count_today;
            }

            if ( disp_count_all != count_data.count_all )
            {
                if (FormWindowState.Minimized != this.WindowState)
                {
                    double in_mb = count_data.count_all % (GB) / 1024.0 / 1024.0;
                    UInt64 in_gb = count_data.count_all / (GB);
                    if (in_gb != 0)
                        CountAll.Text = in_gb + " GB | " + in_mb.ToString("0") + " MB";
                    else
                        CountAll.Text = in_mb.ToString("0") + " MB";

                    disp_count_all = count_data.count_all;
                }

                saveFile();
            }

            string w_name;
            if (Wifi.checkWifi(out w_name))
            {
                if (w_name == wifi_name.Text)
                {
                    meter_on = true;
                }
                else if (meter_on)
                {
                    meter_on = false;
                }
            }
            else if (meter_on)
            {
                meter_on = false;
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            quitApp();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //RegistryKey rkApp = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);

            //rkApp.SetValue("SimpleNetMeter", "\"" + Application.ExecutablePath.ToString() + "\"" + " --minimized");
        }

        private void button2_Click(object sender, EventArgs e)
        {
           // RegistryKey rkApp = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
           // rkApp.DeleteValue("SimpleNetMeter", false);
        }


        public static bool ShowInputBox(string text, string caption, out string input)
        {
            bool cancelled = false;
            Form prompt = new Form();
            prompt.Width = 1;
            prompt.Height = 1;
            prompt.AutoSize = true;
            prompt.Text = caption;
            Label textLabel = new Label() { Left = 10, Top = 10, Text = text, AutoSize = true };
            TextBox textBox = new TextBox() { Left = 10, Top = 40, Width = 220 };
            Button confirmation = new Button() { Text = "Ok", Left = 50, Width = 50, Top = 70 };
            Button cancel_b = new Button() { Text = "Cancel", Left = 125, Width = 50, Top = 70 };
            confirmation.Click += (sender, e) => { prompt.Close(); };
            cancel_b.Click += (sender, e) => { cancelled = true; prompt.Close(); };
            prompt.Controls.Add(confirmation);
            prompt.Controls.Add(cancel_b);
            prompt.Controls.Add(textLabel);
            prompt.Controls.Add(textBox);
            prompt.ShowDialog();

            input = textBox.Text;

            return (!cancelled);
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            string input;
            bool ret = ShowInputBox("Add meg a mérendő wifi hálózat nevét.", "Wifi hálózat neve", out input);
            if (ret)
                wifi_name.Text = input;
        }

        private static string GetStringForSSID(Wlan.Dot11Ssid ssid)
        {
            return Encoding.ASCII.GetString(ssid.SSID, 0, (int)ssid.SSIDLength);
        }



        public static void wlanConnectionChangeHandler(Wlan.WlanNotificationData notifyData, Wlan.WlanConnectionNotificationData connNotifyData)
        {
            try
            {
                //string msg = String.Empty;

                switch (notifyData.notificationSource)
                {
                    case Wlan.WlanNotificationSource.ACM:

                        switch ((Wlan.WlanNotificationCodeAcm)notifyData.notificationCode)
                        {
                            case Wlan.WlanNotificationCodeAcm.ConnectionStart:
                                //msg = "ConnectionStart";
                                break;

                            case Wlan.WlanNotificationCodeAcm.ConnectionComplete:
                                //msg = "ConnectionComplete";
                                WlanClient client = new WlanClient();
                                foreach (WlanClient.WlanInterface wlanIface in client.Interfaces)
                                {
                                    Wlan.WlanAssociationAttributes conAttributes = wlanIface.CurrentConnection.wlanAssociationAttributes;
                                    Wlan.Dot11Ssid ssid = conAttributes.dot11Ssid;
                                    //PhysicalAddress bssid = conAttributes.Dot11Bssid;
                                    //int rssi = wlanIface.RSSI;

                                    if ( Form1.the_form.wifi_name.Text == GetStringForSSID(ssid))
                                    {
                                        Form1.the_form.meter_on = true;
                                    }

                                    //msg += ". ssid: " + GetStringForSSID(ssid) + ". rssi: " + rssi.ToString();//+ ". MAC: " + bssid.ToString();
                                    break;
                                }

                                break;

                            case Wlan.WlanNotificationCodeAcm.Disconnecting:
                                //msg = "Disconnecting";
                                break;

                            case Wlan.WlanNotificationCodeAcm.Disconnected:
                                Form1.the_form.meter_on = false;
                                //msg = "Disconnected";
                                break;

                            default:
                                //msg = "unknown notificationCode =" + notifyData.notificationCode;
                                break;

                        }
                        //Console.WriteLine(msg + " for profile:" + connNotifyData.profileName);
                        break;

                    default:
                        //Console.WriteLine("irrelevant notification. Ignore");
                        break;
                }
            }
            catch (System.ComponentModel.Win32Exception)
            {
                Console.Write("Exception: Could not connect.");
            }
        }

    }

    public class CountData
    {

        private UInt64 count_today_p = 0;
        public UInt64 count_today
        {
            set
            {
                count_today_p = value;
            }
            get
            {
                return count_today_p;
            }
        }

        private UInt64 count_all_p = 0; //bytes
        public UInt64 count_all
        {
            set
            {
                count_all_p = value;
            }
            get
            {
                return count_all_p;
            }
        }
    }



}
