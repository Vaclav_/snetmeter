﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NativeWifi;

namespace SimpleNetMeter
{
    class Wifi
    {
        private static WlanClient client = new WlanClient();

        public static bool checkWifi(out string network_name)
        {
            network_name = "";
            foreach (WlanClient.WlanInterface wlanInterface in client.Interfaces)
            {
                try
                {
                    Wlan.Dot11Ssid ssid = wlanInterface.CurrentConnection.wlanAssociationAttributes.dot11Ssid;
                    network_name = new String(Encoding.ASCII.GetChars(ssid.SSID, 0, (int)ssid.SSIDLength));
                    Console.WriteLine("checkWifi: connected to: " + network_name);
                }
                catch (System.ComponentModel.Win32Exception)
                {
                    Console.WriteLine("Wifi is not connected.");
                    return false;
                }
            }

            //connected
            return true;
        }

        public static void registerWlanListener()
        {
            
            foreach (WlanClient.WlanInterface wlanIface in client.Interfaces)
            {
                //string str = "Name=" + wlanIface.InterfaceName + ". State: ";

                switch (wlanIface.InterfaceState)
                {
                    case Wlan.WlanInterfaceState.NotReady:
                        //str += "NotReady";
                        break;

                    case Wlan.WlanInterfaceState.Disconnected:
                        //str += "Disconnected";
                        break;

                    case Wlan.WlanInterfaceState.Disconnecting:
                        //str += "Disconnecting";
                        break;

                    case Wlan.WlanInterfaceState.Connected:
                        //str += "Connected";
                        break;
                }

                wlanIface.WlanConnectionNotification += Form1.wlanConnectionChangeHandler;
                Console.WriteLine(". Listener registered");
            }
        }

        public static void unregisterWlanListener()
        {
            WlanClient client = new WlanClient();

            try
            {
                foreach (WlanClient.WlanInterface wlanIface in client.Interfaces)
                {
                    wlanIface.WlanConnectionNotification -= Form1.wlanConnectionChangeHandler;
                    Console.WriteLine(wlanIface.InterfaceName + ". Listener unregistered");
                }
            }
            catch (System.ComponentModel.Win32Exception)
            {
                Console.WriteLine("error.");
            }
        }

    }
}
